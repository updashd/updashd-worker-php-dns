<?php
namespace Updashd\Worker;

use Updashd\Configlib\Config;
use Updashd\Configlib\Validator\HostnameValidator;
use Updashd\Worker\Exception\WorkerConfigurationException;

class Dns implements WorkerInterface {
    const CONFIG_FIELD_QUERY_STRING = 'query_string';
    const CONFIG_FIELD_RECORD_TYPE = 'record_type';
    const CONFIG_FIELD_CHECK_ONLY = 'check_only';

    const CONFIG_RECORD_TYPE_OPTION_A = 'A';
    const CONFIG_RECORD_TYPE_OPTION_CNAME = 'CNAME';
    const CONFIG_RECORD_TYPE_OPTION_MX = 'MX';
    const CONFIG_RECORD_TYPE_OPTION_NS = 'NS';
    const CONFIG_RECORD_TYPE_OPTION_PTR = 'PTR';
    const CONFIG_RECORD_TYPE_OPTION_SOA = 'SOA';
    const CONFIG_RECORD_TYPE_OPTION_TXT = 'TXT';
    const CONFIG_RECORD_TYPE_OPTION_AAAA = 'AAAA';
    const CONFIG_RECORD_TYPE_OPTION_SRV = 'SRV';
    const CONFIG_RECORD_TYPE_OPTION_NAPTR = 'NAPTR';
    const CONFIG_RECORD_TYPE_OPTION_HINFO = 'HINFO';
    const CONFIG_RECORD_TYPE_OPTION_A6 = 'A6';

    const METRIC_REMAINING_TTL = 'remaining_ttl';
    const METRIC_RECORD_COUNT = 'record_count';
    const METRIC_A_IP = 'A_ip';
    const METRIC_MX_TARGET = 'MX_target';
    const METRIC_MX_PRIORITY = 'MX_pri';
    const METRIC_CNAME_TARGET = 'CNAME_target';
    const METRIC_NS_TARGET = 'NS_target';
    const METRIC_PTR_TARGET = 'PTR_target';
    const METRIC_TXT_TXT = 'TXT_txt';
    const METRIC_SOA_MNAME = 'SOA_mname';
    const METRIC_SOA_RNAME = 'SOA_rname';
    const METRIC_SOA_SERIAL = 'SOA_serial';
    const METRIC_SOA_REFRESH = 'SOA_refresh';
    const METRIC_SOA_RETRY = 'SOA_retry';
    const METRIC_SOA_EXPIRE = 'SOA_expire';
    const METRIC_SOA_MINIMUM_TTL = 'SOA_minimum-ttl';
    const METRIC_AAAA_IPV6 = 'AAAA_ipv6';
    const METRIC_SRV_PRIORITY = 'SRV_pri';
    const METRIC_SRV_WEIGHT = 'SRV_weight';
    const METRIC_SRV_TARGET = 'SRV_target';
    const METRIC_SRV_PORT = 'SRV_port';
    const METRIC_NAPTR_ORDER = 'NAPTR_order';
    const METRIC_NAPTR_PREF = 'NAPTR_pref';
    const METRIC_NAPTR_FLAGS = 'NAPTR_flags';
    const METRIC_NAPTR_SERVICES = 'NAPTR_services';
    const METRIC_NAPTR_REGEX = 'NAPTR_regex';
    const METRIC_NAPTR_REPLACEMENT = 'NAPTR_replacement';
    const METRIC_HINFO_CPU = 'HINFO_cpu';
    const METRIC_HINFO_OS = 'HINFO_os';
    const METRIC_A6_MASKLEN = 'A6_masklen';
    const METRIC_A6_IPV6 = 'A6_ipv6';
    const METRIC_A6_CHAIN = 'A6_chain';

    protected $config;

    public static function getReadableName () {
        return 'DNS';
    }

    public static function getServiceName () {
        return 'dns';
    }

    /**
     * Create and return a Config object for this service
     * @return Config
     */
    public static function createConfig () {
        $config = new Config();

        $config->addFieldText(self::CONFIG_FIELD_QUERY_STRING, 'Host Name', null, true)
            ->addValidator(new HostnameValidator());

        $config->addFieldSelect(self::CONFIG_FIELD_RECORD_TYPE, 'Record Type', self::getRecordTypes(), self::CONFIG_RECORD_TYPE_OPTION_A, true);

        $config->addFieldCheckbox(self::CONFIG_FIELD_CHECK_ONLY, 'Check Only', true, true);

        return $config;
    }

    /**
     * Get the PHP representation of the given record type
     * @param string $type Eg. A, CNAME, MX, etc
     * @return mixed
     * @throws WorkerConfigurationException
     */
    protected static function getRecordTypeForPhp ($type) {
        $types = [
            'A' => DNS_A,
            'CNAME' => DNS_CNAME,
            'MX' => DNS_MX,
            'NS' => DNS_NS,
            'PTR' => DNS_PTR,
            'SOA' => DNS_SOA,
            'TXT' => DNS_TXT,
            'AAAA' => DNS_AAAA,
            'SRV' => DNS_SRV,
            'NAPTR' => DNS_NAPTR,
            'HINFO' => DNS_HINFO,
            'A6' => DNS_A6,
        ];

        if (! array_key_exists($type, $types)) {
            throw new WorkerConfigurationException('Specified record type is not supported by PHP.');
        }

        return $types[$type];
    }

    protected static function getRecordTypes () {
        // DNS_A, DNS_CNAME, DNS_HINFO, DNS_MX, DNS_NS, DNS_PTR, DNS_SOA, DNS_TXT, DNS_AAAA, DNS_SRV, DNS_NAPTR, DNS_A6, DNS_ALL
        return [
            self::CONFIG_RECORD_TYPE_OPTION_A => 'IPv4 Address (A)',
            self::CONFIG_RECORD_TYPE_OPTION_CNAME => 'Alias (CNAME)',
            self::CONFIG_RECORD_TYPE_OPTION_MX => 'Mail Exchanger (MX)',
            self::CONFIG_RECORD_TYPE_OPTION_NS => 'Name Server (NS)',
            self::CONFIG_RECORD_TYPE_OPTION_PTR => 'Reverse Lookup (PTR)',
            self::CONFIG_RECORD_TYPE_OPTION_SOA => 'Start of Authority (SOA)',
            self::CONFIG_RECORD_TYPE_OPTION_TXT => 'Text (TXT)',
            self::CONFIG_RECORD_TYPE_OPTION_AAAA => 'IPv6 Address (AAAA)',
            self::CONFIG_RECORD_TYPE_OPTION_SRV => 'Service (SRV)',
            self::CONFIG_RECORD_TYPE_OPTION_NAPTR => 'Naming Authority Pointer (NAPTR)',
            self::CONFIG_RECORD_TYPE_OPTION_HINFO => 'CPU and Operating System (HINFO)',
            self::CONFIG_RECORD_TYPE_OPTION_A6 => 'Historic IPv6 (A6)',
        ];
    }

    /**
     * Create and return a Result object for this service
     * @return Result
     */
    public static function createResult () {
        $result = new Result();

        // All Queries come back with this
        $result->addMetricInt(self::METRIC_REMAINING_TTL, 'Remaining TTL', 'sec');
        $result->addMetricInt(self::METRIC_RECORD_COUNT, 'Record Count');

        // A records
        $result->addMetricString(self::METRIC_A_IP, 'IP (A)');

        // MX records
        $result->addMetricString(self::METRIC_MX_TARGET, 'Target (MX)');
        $result->addMetricInt(self::METRIC_MX_PRIORITY, 'Priority (MX)');

        // CNAME records
        $result->addMetricString(self::METRIC_CNAME_TARGET, 'Target (CNAME)');

        // NS records
        $result->addMetricString(self::METRIC_NS_TARGET, 'Target (NS)');

        // PTR records
        $result->addMetricString(self::METRIC_PTR_TARGET, 'Target (PTR)');

        // TXT records
        $result->addMetricString(self::METRIC_TXT_TXT, 'Text (TXT)');

        // SOA records
        $result->addMetricString(self::METRIC_SOA_MNAME, 'Source FQDN (SOA)');
        $result->addMetricString(self::METRIC_SOA_RNAME, 'Email Address (SOA)');
        $result->addMetricInt(self::METRIC_SOA_SERIAL, 'Serial Number (SOA)');
        $result->addMetricInt(self::METRIC_SOA_REFRESH, 'Refresh Interval', 'sec');
        $result->addMetricInt(self::METRIC_SOA_RETRY, 'Retry Interval', 'sec');
        $result->addMetricInt(self::METRIC_SOA_EXPIRE, 'Expire', 'sec');
        $result->addMetricInt(self::METRIC_SOA_MINIMUM_TTL, 'Min TTL', 'sec');

        // AAAA (IPv6 records)
        $result->addMetricString(self::METRIC_AAAA_IPV6, 'IPv6 (AAAA)');

        // SRV records
        $result->addMetricInt(self::METRIC_SRV_PRIORITY, 'Priority (SRV)');
        $result->addMetricInt(self::METRIC_SRV_WEIGHT, 'Weight (SRV)');
        $result->addMetricString(self::METRIC_SRV_TARGET, 'Target (SRV)');
        $result->addMetricInt(self::METRIC_SRV_PORT, 'Port (SRV)');

        // NAPTR records
        $result->addMetricInt(self::METRIC_NAPTR_ORDER, 'Order-Priority (NAPTR)');
        $result->addMetricInt(self::METRIC_NAPTR_PREF, 'Pref-Weight (NAPTR)');
        $result->addMetricString(self::METRIC_NAPTR_FLAGS, 'Flags (NAPTR)');
        $result->addMetricString(self::METRIC_NAPTR_SERVICES, 'Services (NAPTR)');
        $result->addMetricString(self::METRIC_NAPTR_REGEX, 'Regex (NAPTR)');
        $result->addMetricString(self::METRIC_NAPTR_REPLACEMENT, 'Replacement (NAPTR)');

        // HINFO records
        $result->addMetricString(self::METRIC_HINFO_CPU, 'CPU (HINFO)');
        $result->addMetricString(self::METRIC_HINFO_OS, 'OS (HINFO)');

        // A6 (historic ipv6)
        $result->addMetricInt(self::METRIC_A6_MASKLEN, 'Mask Len (A6)', 'bits');
        $result->addMetricString(self::METRIC_A6_IPV6, 'IP (A6)');
        $result->addMetricString(self::METRIC_A6_CHAIN, 'Chain (A6)');

        return $result;
    }

    /**
     * Create a worker instance for the given service
     * @param Config $config the Config object used for configuration
     * @throws \Updashd\Worker\Exception\WorkerConfigurationException
     */
    public function __construct (Config $config) {
        $this->setConfig($config);
    }

    /**
     * Run the given test
     * @throws \Updashd\Worker\Exception\WorkerRuntimeException
     * @return Result the results of the test
     */
    public function run () {
        $result = self::createResult();

        $config = $this->getConfig();

        $query = $config->getValueRequired(self::CONFIG_FIELD_QUERY_STRING);
        $typeStr = $config->getValueRequired(self::CONFIG_FIELD_RECORD_TYPE);

        $typePhp = self::getRecordTypeForPhp($typeStr);

        $checkOnly = $config->getValueRequired(self::CONFIG_FIELD_CHECK_ONLY);

        if ($checkOnly) {
            $isFound = dns_check_record($query, $typeStr);

            if ($isFound) {
                $result->setStatus($result::STATUS_SUCCESS);
            }
            else {
                $result->setStatus($result::STATUS_CRITICAL);
            }
        }
        else {
            $dnsResults = dns_get_record($query, $typePhp);

            if ($dnsResults === false) {
                $result->setStatus($result::STATUS_CRITICAL);
                $result->setErrorCode(0);
                $result->setErrorMessage('PHP dns_get_record failed.');
            }
            elseif (is_array($dnsResults) && count($dnsResults)) {
                $result->setStatus($result::STATUS_SUCCESS);

                $result->setMetricValue(self::METRIC_RECORD_COUNT, count($dnsResults));

                // For now, only use the first result
                $firstResult = $dnsResults[0];

                $type = $firstResult['type'];

                $remainingTtl = $firstResult['ttl'];

                $result->setMetricValue(self::METRIC_REMAINING_TTL, $remainingTtl);

                // Set all the metrics that are available from the result
                foreach ($firstResult as $key => $value) {
                    $metricKey = $type . '_' . $key;

                    $metric = $result->getMetric($metricKey, false);

                    if ($metric) {
                        $metric->setValue($value);
                    }
                }
            }
            else {
                $result->setStatus($result::STATUS_WARNING);
                $result->setErrorCode(0);
                $result->setErrorMessage('No Records Returned');
            }
        }

        return $result;
    }

    /**
     * @return Config
     */
    public function getConfig () {
        return $this->config;
    }

    /**
     * @param Config $config
     */
    public function setConfig ($config) {
        $this->config = $config;
    }
}