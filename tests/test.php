<?php
use Updashd\Worker\Dns;

require __DIR__ . '/../vendor/autoload.php';


$config = Dns::createConfig();

$config->setValue(Dns::CONFIG_FIELD_QUERY_STRING, 'hello.updashd.com');
$config->setValue(Dns::CONFIG_FIELD_CHECK_ONLY, false);
$config->setValue(Dns::CONFIG_FIELD_RECORD_TYPE, Dns::CONFIG_RECORD_TYPE_OPTION_TXT);

$dnsWorker = new Dns($config);

$result = $dnsWorker->run();

print_r($result->toArray());